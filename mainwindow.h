#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sdljoy.h"
#include "axisitem.h"
#include "options.h"
#include "caterpillardriver.h"

#include <QMainWindow>
#include <QTimer>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Options *o, QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer *m_timer;
    JoystickInput *m_joy;
    CaterpillarDriver *m_caterpillar;

    void rescaleScene();
    void sendMotorCommandControl(int x, int y, int yaw, int axis_max);
protected:
    void resizeEvent(QResizeEvent *event);
    AxisItem *axes;
    Options *m_options;
    ControlStatus m_control;

public slots:
    void joyChanged(JoystickStatus *status);
    void readUpdatedStatus(const ControlStatus *status);
    void enableChanged();
    void zeroJog();
    void debugSimulatedEvent();
    void setLEDStatus();
    void performLEDPulse();
    void setVideoFeed();
    void shootStillSequence();
    void openUrl_feed();
    void openUrl_still1();
    void openUrl_still2();
    void openUrl_still3();
    void openUrl_still4();
};

#endif // MAINWINDOW_H
