#ifndef AXISITEM_H
#define AXISITEM_H

#include <QGraphicsItem>
#include <QPainter>
#include "sdljoy.h"
#include "options.h"

class AxisItem : public QGraphicsObject
{
    Q_OBJECT
public:
    AxisItem(Options *o, QGraphicsItem *parent = Q_NULLPTR);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);
    QRectF boundingRect() const;

    void setAxisRangeX(int min, int max, int zero);
    void setAxisRangeYS(int min, int max, int zero);
    void setAxisRangeY(int min, int max, int zero);
    void setAxisRangeYaw(int min, int max, int zero);
    void setInfo(int rpm1, int rpm2, int rpm3, int rpm4);

private:
    Options *m_options;
    bool en1;
    bool en2;
    bool en3;
    bool en4;
    int m_rpm1;
    int m_rpm2;
    int m_rpm3;
    int m_rpm4;

    int m_x_min;
    int m_x0;
    int m_x_max;
    int m_x;

    int m_y_min;
    int m_y0;
    int m_y_max;
    int m_y;

    int m_yaw_min;
    int m_yaw0;
    int m_yaw_max;
    int m_yaw;

    int m_x_actual;
    int m_y_actual;
    int m_yaw_actual;

public:
    void setAxisData(int x, int y, int yaw);
protected:
    void drawHorizBar(QPainter *painter, int min, int max, int zero, int i, bool dotted = false, int y_skew = 0);
    void drawVertBar(QPainter *painter, int min, int max, int zero, int i, bool dotted = false, int x_skew = 0);
    void drawYawBar(QPainter *painter, int min, int max, int zero, int i, bool dotted = false);
    void drawLettering(QPainter *painter);
};

#endif // AXISITEM_H
