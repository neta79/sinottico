#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <QUdpSocket>
#include <QTimer>

namespace Ui {
class OptionsDialog;
}

struct Options;

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    int exec(Options *o);

private:
    Ui::OptionsDialog *ui;
};


struct Options  {
    bool enable1;
    bool enable2;
    bool enable3;
    bool enable4;
    bool enableInvZ;
    bool invert1;
    bool invert2;
    bool invert3;
    bool invert4;
    unsigned int maxrpm1;
    unsigned int maxrpm2;
    unsigned int maxrpm3;
    unsigned int maxrpm4;
    unsigned int accel1;
    unsigned int accel2;
    unsigned int accel3;
    unsigned int accel4;
    unsigned int decel1;
    unsigned int decel2;
    unsigned int decel3;
    unsigned int decel4;
    unsigned int map_to[4];
    QString host;
    unsigned int port;
    unsigned int video_api_port;
    int jog_zero_x;
    int jog_zero_y;
    int jog_zero_yaw;

    void load();
    void persist();
};

#endif // OPTIONS_H
