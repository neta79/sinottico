#include "video.h"
#include <QDebug>

VideoSupport::VideoSupport(const std::string &source):
    m_source(source),
    m_ctx_input(nullptr)
{
}

VideoSupport::~VideoSupport()
{
    if (m_ctx_input)
    {
        avformat_close_input(&m_ctx_input);
        m_ctx_input = nullptr;
    }

}

void VideoSupport::open_input()
{
    if(avformat_open_input(&m_ctx_input, m_source.c_str(), NULL, NULL) < 0)
    {
        throw VideoError("unable to open source stream");
    }
    qDebug() << "stream successfully opened: "<< m_source;

    // Retrieve stream information
    if(avformat_find_stream_info(m_ctx_input, NULL)<0)
    {
        throw VideoError("unable to decode stream infos");
    }

    av_dump_format(m_ctx_input, 0, m_source.c_str(), 0);


    auto get_video_stream_id = [=](){
        auto stream = m_ctx_input->streams;
        auto end = stream + m_ctx_input->nb_streams;
        int i = 0;
        for (; stream!=end; ++stream, ++i)
        {
            if ((*stream)->codec->codec_type==AVMEDIA_TYPE_VIDEO)
            {
                return i;
            }
        }
        return -1;
    };

    auto vsid = look_for_video_stream();
    if (vsid < 0) {
        return -1;
    }

    qDebug() << "video stream found at id = "<< vsid;

    // Get a pointer to the codec context for the video stream

    AVCodecContext *pCodecCtx = pFormatCtx->streams[vsid]->codec;


    AVCodec *pCodec = avcodec_find_decoder(pCodecCtx->codec_id);

    if(pCodec==NULL) {
      qDebug() << "Unsupported codec!";
      return -1;
    }

    qDebug() << "codec found:" << pCodec->name;

    pCodecCtx = avcodec_alloc_context3(pCodec);

    qDebug() << "codec context created";

    AVCodecParameters *params = avcodec_parameters_alloc();
    if (avcodec_parameters_from_context(params, pCodecCtx) < 0)
    {
        qDebug() << "fail: avcodec_parameters_from_context()";
        return -1;
    }

    qDebug() << "codec params copied from source render context";

    AVCodecContext *myCtx = avcodec_alloc_context3(pCodec);
    if (myCtx == nullptr)
    {
        qDebug() << "fail: avcodec_alloc_context3()";
        return -1;
    }

    qDebug() << "allocated new rendercontext";

    if (avcodec_parameters_to_context(myCtx, params) < 0)
    {
        qDebug() << "fail: avcodec_parameters_to_context()";
        return -1;
    }

    qDebug() << "codec params copied to new render context";


//    if(avcodec_copy_context(pCodecCtxOrig, pCodecCtx) != 0) {
//      qDebug() << "Couldn't copy codec context!";
//      return -1;
//    }

    // Open codec
    if(avcodec_open2(myCtx, pCodec, nullptr)<0)
    {
        qDebug() << "Could not open codec";
        return -1;
    }

    qDebug() << "opened new render context";


    AVFrame *pFrame = av_frame_alloc();

    if(pFrame == nullptr)
    {
        qDebug() << "fail: av_frame_alloc()";
        return -1;
    }

    qDebug() << "allocated new video frame";




//    int i;
//    AVCodecContext *pCodecCtxOrig = NULL;
//    AVCodecContext *pCodecCtx = NULL;

//    // Find the first video stream
//    videoStream=-1;
//    for(i=0; i<pFormatCtx->nb_streams; i++)
//      if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
//        videoStream=i;
//        break;
//      }
//    if(videoStream==-1)
//      return -1; // Didn't find a video stream

//    // Get a pointer to the codec context for the video stream
//    pCodecCtx=pFormatCtx->streams[videoStream]->codec;

    return 0;

}
