#include "sdljoy.h"


JoystickInput::JoystickInput(QObject *parent):
    QObject(parent),
    m_device(nullptr),
    m_gave_warning(false)
{
    qDebug() << "initializing SDL layer";
    SDL_Init(SDL_INIT_JOYSTICK|SDL_INIT_EVENTS);
}


void JoystickInput::selectDevice()
{
    for (int i = 0; i < SDL_NumJoysticks(); ++i)
    {
        auto joy = SDL_JoystickOpen(i);

        if (joy) {
            qDebug() << "opened joystick device" << i;
            qDebug() << "device name:" << SDL_JoystickName(joy);
            qDebug() << "number of axes:" << SDL_JoystickNumAxes(joy);
            qDebug() << "number of buttons:" << SDL_JoystickNumButtons(joy);
            if (SDL_JoystickNumAxes(joy) < JOY_AXES_MIN)
            {
                qDebug() << "not enough axes. device" << i << "ignored";
                continue;
            }
            if (SDL_JoystickNumButtons(joy) < JOY_BUTTONS_MIN)
            {
                qDebug() << "not enough buttons. device" << i << "ignored";
                continue;
            }
            m_device = joy;
            break;
        } else {
            qWarning() << "couldn't open joystick device" << i;
        }
    }
}


JoystickInput::~JoystickInput()
{
    qDebug() << "terminating SDL layer";
    SDL_Quit();
}


void JoystickInput::readDevice()
{
    if (m_device == nullptr)
    {
        if (!m_gave_warning)
        {
            m_gave_warning = true;
            qWarning() << "unable to detect suitable joystick unit";
        }
        return;
    }
    JoystickStatus status;
    SDL_JoystickUpdate();
    status.axis_1 = SDL_JoystickGetAxis(m_device, 0);
    status.axis_2 = SDL_JoystickGetAxis(m_device, 1);
    status.axis_3 = SDL_JoystickGetAxis(m_device, 2);
    status.btn1   = SDL_JoystickGetButton(m_device, 0);
    status.btn2   = SDL_JoystickGetButton(m_device, 1);
    status.btn3   = SDL_JoystickGetButton(m_device, 2);
    status.btn4   = SDL_JoystickGetButton(m_device, 3);
    status.btn5   = SDL_JoystickGetButton(m_device, 4);
    status.btn6   = SDL_JoystickGetButton(m_device, 5);
    status.btn7   = SDL_JoystickGetButton(m_device, 6);
    status.btn8   = SDL_JoystickGetButton(m_device, 7);
    bool axev = false, btnev = false;
    if (m_last.axisHasChanged(status))
    {
        axev = true;
    }
    if (m_last.buttonHasChanged(status))
    {
        btnev = true;
    }
    m_last = status;
    if (axev)
    {
        emit axisHasChanged(&m_last);
    }
    if (btnev)
    {
        emit buttonPressed(&m_last);
    }
}


const JoystickStatus *JoystickInput::last() const
{
    return &m_last;
}
