#ifndef COMMANDS_H
#define COMMANDS_H

#include <QByteArray>

struct CommandFactory {
    CommandFactory();
    CommandFactory(const CommandFactory&) = delete;

    void get_version(QByteArray &data);
    void init_driver(QByteArray &data, quint8 driver, quint16 aramp, quint16 dramp, quint16 max_rpm);
    void enable_driver(QByteArray &data, quint8 driver, bool active);
    void set_rpm(QByteArray &data, quint8 driver, qint16 rpm);
    void enable_12v_rail(QByteArray &data, bool active);
    void set_leds(QByteArray &data, unsigned int mask);
    void set_video_feed(QByteArray &data,
                        bool active,
                        quint8 camera,
                        bool vflip, bool hflip, quint16 width,
                        quint16 height,
                        quint8 fps_num, quint8 fps_den,
                        quint8 shutter_num, quint8 shutter_den,
                        quint16 iso,
                        quint8 settle_time_s
                        , bool record);
    void shoot_still_sequence(QByteArray &data,
                              quint8 mode,
                              bool vflip, bool hflip,
                              quint16 width, quint16 height,
                              quint8 fps_num, quint8 fps_den,
                              quint8 shutter_num, quint8 shutter_den,
                              quint16 iso,
                              quint8 settle_time_s, quint8 format,
                              unsigned int pulses,
                              unsigned int pulse_len,
                              unsigned int pulse_del,
                              quint8 camera0,
                              quint16 camera0_leds,
                              quint8 camera1,
                              quint16 camera1_leds,
                              quint8 camera2,
                              quint16 camera2_leds,
                              quint8 camera3,
                              quint16 camera3_leds,
                              quint8 camera4,
                              quint16 camera4_leds,
                              quint8 camera5,
                              quint16 camera5_leds,
                              quint8 camera6,
                              quint16 camera6_leds,
                              quint8 camera7,
                              quint16 camera7_leds
                              );
    void pulse_leds(QByteArray &data, unsigned int mask, unsigned int pulses,
                                    unsigned int pulse_len, unsigned int pulse_del);
private:
    quint32 get_seq();

    quint32 m_ctr;
};


#define STILL_SEQUENCE_STILL_PORT 0x01
#define STILL_SEQUENCE_VIDEO_PORT 0x02
#define STILL_SEQUENCE_LONG_EXPOSURE 0x03


#endif // COMMANDS_H
