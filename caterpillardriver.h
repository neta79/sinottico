#ifndef CATERPILLARDRIVER_H
#define CATERPILLARDRIVER_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include "options.h"
#include "commands.h"
#include <QDateTime>

struct ControlStatus {
    int m1rpm;
    int m2rpm;
    int m3rpm;
    int m4rpm;
    bool m1en;
    bool m2en;
    bool m3en;
    bool m4en;
    bool m1hold;
    bool m2hold;
    bool m3hold;
    bool m4hold;
    bool en12v;
};

class CaterpillarDriver : public QObject
{
    Q_OBJECT
public:
    explicit CaterpillarDriver(QObject *parent, Options *o);
    CaterpillarDriver(const CaterpillarDriver &) = delete;
    ~CaterpillarDriver();

    void updateControl(const ControlStatus *control, bool fast=false);
    void set_leds(unsigned int mask);
    void pulse_leds(unsigned int mask, unsigned int pulses,
                    unsigned int pulse_len, unsigned int pulse_del);

    void set_video_feed(bool active,
                        quint8 camera,
                        bool vflip, bool hflip, quint16 width,
                        quint16 height,
                        quint8 fps_num, quint8 fps_den,
                        quint8 shutter_num, quint8 shutter_den,
                        quint16 iso,
                        quint8 settle_time_s
                        , bool record);
    void shoot_still_sequence(quint8 mode, bool vflip, bool hflip,
                              quint16 width, quint16 height,
                              quint8 fps_num, quint8 fps_den,
                              quint8 shutter_num, quint8 shutter_den,
                              quint16 iso,
                              quint8 settle_time_s, quint8 format,
                              unsigned int pulses,
                              unsigned int pulse_len,
                              unsigned int pulse_del,
                              quint8 camera0,
                              quint16 camera0_leds,
                              quint8 camera1,
                              quint16 camera1_leds,
                              quint8 camera2,
                              quint16 camera2_leds,
                              quint8 camera3,
                              quint16 camera3_leds,
                              quint8 camera4,
                              quint16 camera4_leds,
                              quint8 camera5,
                              quint16 camera5_leds,
                              quint8 camera6,
                              quint16 camera6_leds,
                              quint8 camera7,
                              quint16 camera7_leds
                              );

signals:
    void statusChanged(const ControlStatus *control);

private:
    Options *m_options;
    QUdpSocket *m_socket;
    ControlStatus *m_control_last;
    CommandFactory *m_factory;
    void sendCommand(QByteArray cmd);
    QTimer *m_delay_timer;
    void rearm_timer();
    void sendDelayedCommand();
    QByteArray cache_m1;
    QByteArray cache_m2;
    QByteArray cache_m3;
    QByteArray cache_m4;
};

#endif // CATERPILLARDRIVER_H
