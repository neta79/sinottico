#include "axisitem.h"
#include <QGraphicsScene>
#include <QFontDatabase>

/*AxisItem::AxisItem(QObject *parent) : QObject(parent)
{

}*/

#define PEN_SIZE .5
#define HBS 8

AxisItem::AxisItem(Options *o, QGraphicsItem *parent):
    m_options(o),
    en1(false),
    en2(false),
    en3(false),
    en4(false),
    m_rpm1(0),
    m_rpm2(0),
    m_rpm3(0),
    m_rpm4(0)
{
}

void AxisItem::drawHorizBar(QPainter *painter, int min, int max, int zero, int i, bool dotted, int y_skew)
{
    auto imin = min;
    auto imax = max;
    int sign = 0;
    double pct = 0;
    if (i > zero)
    {
        if (i>imax) i=imax;
        sign = 1;
        pct = ((double)(i-zero)) / ((double)(imax-zero));
    }
    if (i < zero)
    {
        if (i<imin) i=imin;
        sign = -1;
        pct = ((double)(zero-i)) / ((double)(zero-imin));
    }

    QRectF rect = boundingRect();
    auto midx = rect.width()/2;
    auto midy = rect.height()/2;
    auto max_x = rect.width();
    auto max_y = rect.width();

    auto max_shaft = midx-HBS;
    auto shaft = max_shaft * pct;


    int start = midx;
    int end = midx;
    int other = midy;
    if (sign>0)
        end += shaft;
    if (sign<0)
        start -= shaft;
    other += y_skew;

    QPolygon pol;
    pol.append(QPoint(start, other-HBS));
    pol.append(QPoint(start-HBS, other));
    pol.append(QPoint(start, other+HBS));

    pol.append(QPoint(end, other+HBS));
    pol.append(QPoint(end+HBS, other));
    pol.append(QPoint(end, other-HBS));

    QPen penSmall(Qt::green, PEN_SIZE);
    penSmall.setStyle(dotted ? Qt::DotLine : Qt::SolidLine);

    painter->setPen(penSmall);
    painter->drawPolygon(pol);
}


void AxisItem::drawVertBar(QPainter *painter, int min, int max, int zero, int i, bool dotted, int x_skew)
{
    auto imin = min;
    auto imax = max;
    int sign = 1;
    double pct = 0;
    if (i > zero)
    {
        if (i>imax) i=imax;
        sign = 1;
        pct = ((double)(i-zero)) / ((double)(imax-zero));
    }
    if (i < zero)
    {
        if (i<imin) i=imin;
        sign = -1;
        pct = ((double)(zero-i)) / ((double)(zero-imin));
    }

    QRectF rect = boundingRect();
    auto midx = rect.width()/2;
    auto midy = rect.height()/2;
    auto max_x = rect.width();
    auto max_y = rect.width();

    auto max_shaft = midy-HBS;
    auto shaft = max_shaft * pct;


    int start = midy;
    int end = midy;
    int other = midx;
    if (sign>0)
        end += shaft;
    if (sign<0)
        start -= shaft;
    other += x_skew;

    QPolygon pol;
    pol.append(QPoint(other-HBS, start));
    pol.append(QPoint(other, start-HBS));
    pol.append(QPoint(other+HBS, start));

    pol.append(QPoint(other+HBS, end));
    pol.append(QPoint(other, end+HBS));
    pol.append(QPoint(other-HBS, end));

    QPen penSmall(Qt::red, PEN_SIZE);
    penSmall.setStyle(dotted ? Qt::DotLine : Qt::SolidLine);

    painter->setPen(penSmall);
    painter->drawPolygon(pol);
}


void AxisItem::drawYawBar(QPainter *painter, int min, int max, int zero, int i, bool dotted)
{
    auto imin = min;
    auto imax = max;
    int sign = 1;
    double pct = 0;
    if (i > zero)
    {
        if (i>imax) i=imax;
        sign = 1;
        pct = ((double)(i-zero)) / ((double)(imax-zero));
    }
    if (i < zero)
    {
        if (i<imin) i=imin;
        sign = -1;
        pct = ((double)(zero-i)) / ((double)(zero-imin));
    }

    QRectF rect = boundingRect();
    auto midx = rect.width()/2;
    auto midy = rect.height()/2;
    auto max_x = rect.width();
    auto max_y = rect.width();

    QPen penSmall(Qt::cyan, PEN_SIZE);
    penSmall.setStyle(dotted ? Qt::DotLine : Qt::SolidLine);

    int angle = 90 * pct;
    int start = 90;
    if (sign < 0)
    {
        start += angle;
    }


    painter->setPen(penSmall);
    painter->drawArc(QRectF(0, 0, max_x, max_y), start*16, -angle*16);
    painter->drawArc(QRectF(HBS*2, HBS*2, max_x-HBS*4, max_y-HBS*4), start*16, -angle*16);

    QPolygon pol;
    if (sign > 0)
    {
        pol.append(QPoint(midx, 0));
        pol.append(QPoint(midx-HBS, HBS));
        pol.append(QPoint(midx, HBS*2));
    } else {
        pol.append(QPoint(midx, 0));
        pol.append(QPoint(midx+HBS, HBS));
        pol.append(QPoint(midx, HBS*2));
    }
    painter->drawPolyline(pol);
    pol.clear();

    if (sign > 0)
    {
        pol.append(QPoint(0, -midy));
        pol.append(QPoint(0+HBS, HBS-midy));
        pol.append(QPoint(0, HBS*2-midy));
    } else {
        pol.append(QPoint(0, -midy));
        pol.append(QPoint(0-HBS, HBS-midy));
        pol.append(QPoint(0, HBS*2-midy));
    }

    QTransform t = painter->transform();
    QTransform old = t;
    t.translate(midx, midy);
    t.rotate(angle*sign);
    painter->setWorldTransform(t);
    painter->drawPolyline(pol);
    painter->setTransform(old);
}

#define FONT_SIZE 3

void AxisItem::drawLettering(QPainter *painter)
{
    QRectF rect = boundingRect();
    auto max_x = rect.width();
    auto max_y = rect.width();

    QPen penSmall(Qt::yellow, PEN_SIZE);
    penSmall.setStyle(Qt::SolidLine);
    painter->setPen(penSmall);

    QFont f = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    f.setBold(false);
    f.setPixelSize(FONT_SIZE);
    painter->setFont(f);
    auto x = max_x - f.pixelSize()*15;
    auto y = max_x - f.pixelSize()*6;
    QPoint p(x, y);
    QString s;
    bool engaged;
    int mno, rpm;
    s.clear();
    mno = 1;
    engaged = en1;
    rpm = m_rpm1;
    s.sprintf("mot.%d %s: %05d rpm",
              mno, engaged?" ON":"OFF",
              rpm);
    painter->drawText(p, s);

    p.setY(p.y() + f.pixelSize());
    s.clear();
    mno = 2;
    engaged = en2;
    rpm = m_rpm2;
    s.sprintf("mot.%d %s: %05d rpm",
              mno, engaged?" ON":"OFF",
              rpm);
    painter->drawText(p, s);

    p.setY(p.y() + f.pixelSize());
    s.clear();
    mno = 3;
    engaged = en3;
    rpm = m_rpm3;
    s.sprintf("mot.%d %s: %05d rpm",
              mno, engaged?" ON":"OFF",
              rpm);
    painter->drawText(p, s);

    p.setY(p.y() + f.pixelSize());
    s.clear();
    mno = 4;
    engaged = en4;
    rpm = m_rpm4;
    s.sprintf("mot.%d %s: %05d rpm",
              mno, engaged?" ON":"OFF",
              rpm);
    painter->drawText(p, s);
}


void AxisItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
//    drawHorizBar(painter, m_x_min, m_x_max, m_x0, m_x_actual, false);
//    drawVertBar(painter, m_y_min, m_y_max, m_y0, m_y_actual, false);
//    drawVertBar(painter, m_ys_min, m_ys_max, m_ys0, m_ys_actual, false, -HBS*3);
//    drawVertBar(painter, m_ys_min, m_ys_max, m_ys0, m_ys_actual, false, HBS*3);
//    drawYawBar(painter, m_yaw_min, m_yaw_max, m_yaw0, m_yaw_actual, false);

//    drawHorizBar(painter, m_x_min, m_x_max, m_x0, m_x, true);
//    drawVertBar(painter, m_y_min, m_y_max, m_y0, m_y, true);
    drawVertBar(painter, m_y_min, m_y_max, m_y0, m_y, true, -HBS*3);
    drawVertBar(painter, m_y_min, m_y_max, m_y0, m_y, true, HBS*3);
    auto m_xa = m_x + m_yaw;
    auto m_xb = m_x - m_yaw;
    drawHorizBar(painter, m_x_min, m_x_max, m_x0, m_xa, true, -HBS*3);
    drawHorizBar(painter, m_x_min, m_x_max, m_x0, m_xb, true, HBS*3);
//    drawYawBar(painter, m_yaw_min, m_yaw_max, m_yaw0, m_yaw, true);

    drawLettering(painter);
}

QRectF AxisItem::boundingRect() const
{
    return QRectF(0, 0, 100, 100);
}

void AxisItem::setAxisRangeX(int min, int max, int zero)
{
    m_x_min = min;
    m_x_max = max;
    m_x0 = zero;
}
void AxisItem::setAxisRangeY(int min, int max, int zero)
{
    m_y_min = min;
    m_y_max = max;
    m_y0 = zero;
}
void AxisItem::setAxisRangeYaw(int min, int max, int zero)
{
    m_yaw_min = min;
    m_yaw_max = max;
    m_yaw0 = zero;
}

void AxisItem::setAxisData(int x, int y, int yaw)
{
    m_x = x;
    m_y = y;
    m_yaw = yaw;
}

void AxisItem::setInfo(int rpm1, int rpm2, int rpm3, int rpm4)
{
    m_rpm1 = rpm1;
    m_rpm2 = rpm2;
    m_rpm3 = rpm3;
    m_rpm4 = rpm4;
}
