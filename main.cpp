#include "mainwindow.h"
#include "options.h"
#include "sdljoy.h"
#include "videooutdialog.h"
#include <QApplication>
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <SDL.h>
#include <SDL_joystick.h>
#include <QMediaPlayer>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#undef main

static int test_av() {
    AVFormatContext *pFormatCtx = nullptr;
    auto fpath = "/home/neta/Projects/_NETA/example.h264";
    if(avformat_open_input(&pFormatCtx, fpath, NULL, NULL)!=0)
    {
        return -1;
    }

    qDebug() << "file opened";

    // Retrieve stream information
    if(avformat_find_stream_info(pFormatCtx, NULL)<0)
    {
        return -1;
    }

    qDebug() << "retrieved stream information";

    av_dump_format(pFormatCtx, 0, fpath, 0);

    auto look_for_video_stream = [=](){
        auto stream = pFormatCtx->streams;
        auto end = stream + pFormatCtx->nb_streams;
        int i = 0;
        for (; stream!=end; ++stream, ++i)
        {
            if ((*stream)->codec->codec_type==AVMEDIA_TYPE_VIDEO)
            {
                return i;
            }
        }
        return -1;
    };

    auto vsid = look_for_video_stream();
    if (vsid < 0) {
        return -1;
    }

    qDebug() << "video stream found at id = "<< vsid;

    // Get a pointer to the codec context for the video stream

    AVCodecContext *pCodecCtx = pFormatCtx->streams[vsid]->codec;


    AVCodec *pCodec = avcodec_find_decoder(pCodecCtx->codec_id);

    if(pCodec==NULL) {
      qDebug() << "Unsupported codec!";
      return -1;
    }

    qDebug() << "codec found:" << pCodec->name;

    pCodecCtx = avcodec_alloc_context3(pCodec);

    qDebug() << "codec context created";

    AVCodecParameters *params = avcodec_parameters_alloc();
    if (avcodec_parameters_from_context(params, pCodecCtx) < 0)
    {
        qDebug() << "fail: avcodec_parameters_from_context()";
        return -1;
    }

    qDebug() << "codec params copied from source render context";

    AVCodecContext *myCtx = avcodec_alloc_context3(pCodec);
    if (myCtx == nullptr)
    {
        qDebug() << "fail: avcodec_alloc_context3()";
        return -1;
    }

    qDebug() << "allocated new rendercontext";

    if (avcodec_parameters_to_context(myCtx, params) < 0)
    {
        qDebug() << "fail: avcodec_parameters_to_context()";
        return -1;
    }

    qDebug() << "codec params copied to new render context";


//    if(avcodec_copy_context(pCodecCtxOrig, pCodecCtx) != 0) {
//      qDebug() << "Couldn't copy codec context!";
//      return -1;
//    }

    // Open codec
    if(avcodec_open2(myCtx, pCodec, nullptr)<0)
    {
        qDebug() << "Could not open codec";
        return -1;
    }

    qDebug() << "opened new render context";


    AVFrame *pFrame = av_frame_alloc();

    if(pFrame == nullptr)
    {
        qDebug() << "fail: av_frame_alloc()";
        return -1;
    }

    qDebug() << "allocated new video frame";




//    int i;
//    AVCodecContext *pCodecCtxOrig = NULL;
//    AVCodecContext *pCodecCtx = NULL;

//    // Find the first video stream
//    videoStream=-1;
//    for(i=0; i<pFormatCtx->nb_streams; i++)
//      if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
//        videoStream=i;
//        break;
//      }
//    if(videoStream==-1)
//      return -1; // Didn't find a video stream

//    // Get a pointer to the codec context for the video stream
//    pCodecCtx=pFormatCtx->streams[videoStream]->codec;

    return 0;
}


int main(int argc, char *argv[])
{
    av_register_all();
    QApplication a(argc, argv);
    QApplication::setOrganizationName("Smrobotica");
    QApplication::setOrganizationDomain("it.smrobotica");
    QApplication::setApplicationName("caterpillar.control.gui");

    Options options;
    options.load();

    OptionsDialog optionsDialog;
    int ok = optionsDialog.exec(&options);
    if (ok)
    {
        options.persist();


        //JoyThread joy_thread;
        //joy_thread.start();

        MainWindow mainWindow(&options);
        mainWindow.show();

        return a.exec();
    }
    return ok != 0;
}
