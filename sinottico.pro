#-------------------------------------------------
#
# Project created by QtCreator 2016-07-01T13:21:28
#
#-------------------------------------------------

QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += link_pkgconfig
PKGCONFIG += sdl2
PKGCONFIG += libavcodec
PKGCONFIG += libswscale
PKGCONFIG += libavformat

TARGET = sinottico
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sdljoy.cpp \
    axisitem.cpp \
    caterproto.cpp \
    options.cpp \
    commands.cpp \
    caterpillardriver.cpp \

#    video.cpp

HEADERS += mainwindow.h \
    sdljoy.h \
    axisitem.h \
    options.h \
    commands.h \
    caterpillardriver.h \

#    video.h

FORMS += mainwindow.ui \
    options.ui

DISTFILES += \
    base1.qml

