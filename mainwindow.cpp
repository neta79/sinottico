#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_options.h"
#include "axisitem.h"
#include "commands.h"

#include <QDebug>
#include <QResizeEvent>
#include <QDesktopServices>

#define LOCAL_BIND_PORT 6789
#define AXIS_MIN (-32768)
#define AXIS_MAX 32767
#define AXIS_NOISE 1000
#define Y_THR_BOOST (AXIS_MAX*2/3)

//#define AXIS_DEBUG 1

#define DEFAULT_SETTLE_TIME 10

static const struct {
    const char *label;
    unsigned int width;
    unsigned int height;
} resolutions[] = {
    {"AUTO", 0, 0},
    {"648x480", 640, 480},
    {"1920x1080", 1920, 1080},
};

static const struct {
    const char *label;
    unsigned int num;
    unsigned int den;
} framerates[] = {
    {"AUTO", 0, 0} ,
    {"60",  60, 1} ,
    {"30",  30, 1} ,
    {"25",  25, 1} ,
    {"24",  24, 1} ,
    {"10",  10, 1} ,
    {"5",   5,  1} ,
    {"3",   3,  1} ,
    {"1",   1,  1} ,
    {"1/2", 1,  3} ,
    {"1/5", 1,  5} ,
    {"1/10",1,  10},
    {"1/20",1,  20},
    {"1/30",1,  30},
};

static const struct {
    const char *label;
    unsigned int id;
} cameras[] = {
    {"Camera_1 (nav)", 1},
    {"Camera_2", 2},
    {"Camera_3", 3},
    {"Camera_4", 4},
    {"Camera_5", 5},
};

static const struct {
    const char *label;
    unsigned int num;
    unsigned int den;
} shutter_settings[] = {
    {"AUTO", 0, 0 },
    {"1/1",  1, 1 },
    {"9/10", 9, 10},
    {"8/10", 8, 10},
    {"5/10", 5, 10},
    {"2/10", 2, 10},
    {"1/10", 1, 10},
};

static void setup_visuals_widgets(Ui::MainWindow *ui) {
    ui->camera_iso->clear();
    ui->camera_iso->addItem("AUTO", 0);
    ui->camera_iso->addItem("ISO_100", 100);
    ui->camera_iso->addItem("ISO_200", 200);
    ui->camera_iso->addItem("ISO_400", 400);
    ui->camera_iso->addItem("ISO_800", 800);

    ui->camera_resolution->clear();
    int ctr = 0;
    for (auto &i: resolutions) {
        ui->camera_resolution->addItem(i.label, ctr);
        ctr ++;
    }

    ui->camera_framerate->clear();
    ctr = 0;
    for (auto &i: framerates) {
        ui->camera_framerate->addItem(i.label, ctr);
        ctr ++;
    }

    ui->camera_shutter->clear();
    ctr = 0;
    for (auto &i: shutter_settings) {
        ui->camera_shutter->addItem(i.label, ctr);
        ctr ++;
    }

    QComboBox *camera_lists[] = {
        ui->video_camera,
        ui->snap1_camera,
        ui->snap2_camera,
        ui->snap3_camera,
        ui->snap4_camera,
    };
    for (auto w: camera_lists) {
        w->clear();
    }
    ui->video_camera->addItem("OFF", 0);
    for (auto w: camera_lists) {
        for (auto &i: cameras) {
            w->addItem(i.label, i.id);
        }
    }

    int id = 1;
    ui->snap1_camera->setCurrentIndex(id++);
    ui->snap2_camera->setCurrentIndex(id++);
    ui->snap3_camera->setCurrentIndex(id++);
    ui->snap4_camera->setCurrentIndex(id++);

    ui->radio_img_format_jpeg->setChecked(true);
}

MainWindow::MainWindow(Options *o, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_options(o)
{
    ui->setupUi(this);
//    ui->quickWidget->setSource(QUrl::fromLocalFile("/home/neta/Projects/_NETA/sinottico/base1.qml"));
    auto w = ui->graphicsView;
    auto scene = new QGraphicsScene(0, 0, 200, 200, w);
    scene->setBackgroundBrush(Qt::black);
    axes = new AxisItem(m_options);
    axes->setAxisRangeX  (AXIS_MIN, AXIS_MAX, 0);
    axes->setAxisRangeY  (AXIS_MIN, AXIS_MAX, 0);
    axes->setAxisRangeYaw(AXIS_MIN, AXIS_MAX, 0);
    scene->addItem(axes);
    w->setScene(scene);

    m_joy = new JoystickInput(this);
    m_joy->selectDevice();

    //m_socket = new QUdpSocket(this);
    //m_socket->bind(QHostAddress::Any, LOCAL_BIND_PORT);

    m_timer = new QTimer(this);
    m_timer->setInterval(50);
    m_timer->setSingleShot(false);

    m_caterpillar = new CaterpillarDriver(this, m_options);

    connect(m_timer, &QTimer::timeout, m_joy, &JoystickInput::readDevice);
    connect(m_timer, &QTimer::timeout, this, [this](){
        rescaleScene();
    });
    connect(m_joy, &JoystickInput::axisHasChanged, this, &MainWindow::joyChanged);
    connect(m_caterpillar, &CaterpillarDriver::statusChanged, this, &MainWindow::readUpdatedStatus);
    connect(ui->enable12v, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->enable1, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->enable2, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->enable3, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->enable4, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->hold1, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->hold2, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->hold3, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->hold4, &QCheckBox::stateChanged, this, &MainWindow::enableChanged);
    connect(ui->btn_zero_jog, &QPushButton::clicked, this, &MainWindow::zeroJog);

    m_timer->start();

    ui->enable1->setChecked( m_options->enable1 );
    ui->enable2->setChecked( m_options->enable2 );
    ui->enable3->setChecked( m_options->enable3 );
    ui->enable4->setChecked( m_options->enable4 );
    ui->hold1->setChecked(false);
    ui->hold2->setChecked(false);
    ui->hold3->setChecked(false);
    ui->hold4->setChecked(false);
    ui->enable12v->setChecked(false);

#if AXIS_DEBUG
    connect(ui->dbgset, &QPushButton::pressed, this, &MainWindow::debugSimulatedEvent);
#else
    ui->dbgx->setHidden(true);
    ui->dbgy->setHidden(true);
    ui->dbgz->setHidden(true);
    ui->dbgset->setHidden(true);
#endif

    setup_visuals_widgets(ui);
    QString s;
    s.clear();
    s.sprintf("http://%s:%d/cam/feed", m_options->host.toLatin1().data(), m_options->video_api_port);
    ui->feed_label->setText(s);

    s.clear();
    s.sprintf("http://%s:%d/cam/still/1", m_options->host.toLatin1().data(), m_options->video_api_port);
    ui->still1_lbl->setText(s);

    s.clear();
    s.sprintf("http://%s:%d/cam/still/2", m_options->host.toLatin1().data(), m_options->video_api_port);
    ui->still2_lbl->setText(s);

    s.clear();
    s.sprintf("http://%s:%d/cam/still/3", m_options->host.toLatin1().data(), m_options->video_api_port);
    ui->still3_lbl->setText(s);

    s.clear();
    s.sprintf("http://%s:%d/cam/still/4", m_options->host.toLatin1().data(), m_options->video_api_port);
    ui->still4_lbl->setText(s);

    connect(ui->led_set_btn, &QPushButton::clicked, this, &MainWindow::setLEDStatus);
    connect(ui->led_pulse_test_btn, &QPushButton::clicked, this, &MainWindow::performLEDPulse);


    auto currentIndexChanged = static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged);
    connect(ui->video_camera, currentIndexChanged, this, &MainWindow::setVideoFeed);
    connect(ui->camera_resolution, currentIndexChanged, this, &MainWindow::setVideoFeed);
    connect(ui->camera_framerate, currentIndexChanged, this, &MainWindow::setVideoFeed);
    connect(ui->camera_shutter, currentIndexChanged, this, &MainWindow::setVideoFeed);
    connect(ui->video_vflip, &QCheckBox::stateChanged, this, &MainWindow::setVideoFeed);
    connect(ui->video_hflip, &QCheckBox::stateChanged, this, &MainWindow::setVideoFeed);
    connect(ui->camera_iso, currentIndexChanged, this, &MainWindow::setVideoFeed);

    connect(ui->shoot_sequence_btn, &QPushButton::clicked, this, &MainWindow::shootStillSequence);

    connect(ui->feed_label, &QPushButton::clicked, this, &MainWindow::openUrl_feed);
    connect(ui->still1_lbl, &QPushButton::clicked, this, &MainWindow::openUrl_still1);
    connect(ui->still2_lbl, &QPushButton::clicked, this, &MainWindow::openUrl_still2);
    connect(ui->still3_lbl, &QPushButton::clicked, this, &MainWindow::openUrl_still3);
    connect(ui->still4_lbl, &QPushButton::clicked, this, &MainWindow::openUrl_still4);

    enableChanged();
}

void MainWindow::rescaleScene()
{
    auto w = ui->graphicsView;
    w->setSceneRect(0, 0, w->frameSize().width(), w->frameSize().height());
    w->fitInView(w->scene()->itemsBoundingRect());
}


void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    rescaleScene();
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendMotorCommandControl(int x, int y, int yaw, int axis_max)
{
    axes->setAxisData(x, y, yaw);

    if (! m_control.en12v)
    {
        return;
    }


    // compute RPM values for drivers 1, 2, 3, 4
    double val_x = (double)x / axis_max;
    double val_y = (double)y / axis_max;
    double val_yaw = (double)yaw / axis_max;
    double omega1 = val_y;
    double omega2 = val_y;
    double omega3 = val_x+val_yaw;
    double omega4 = val_x-val_yaw;

    while (qAbs(omega3) > 1.01)
    {
        omega3 *= 0.99;
    }
    while (qAbs(omega4) > 1.01)
    {
        omega4 *= 0.99;
    }

    auto set_rpm = [&](int motor, double speed) {
        qDebug() << "motor"<<motor<<"mapped to"<<m_options->map_to[motor];
        switch (m_options->map_to[motor])
        {
        case 0:
            if (m_control.m1en && ! m_control.m1hold)
            {
                m_control.m1rpm = speed * m_options->maxrpm1 * (m_options->invert1 ? -1 : 1);
            }
            break;
        case 1:
            if (m_control.m2en && ! m_control.m2hold)
            {
                m_control.m2rpm = speed * m_options->maxrpm2 * (m_options->invert2 ? -1 : 1);
            }
            break;
        case 2:
            if (m_control.m3en && ! m_control.m3hold)
            {
                m_control.m3rpm = speed * m_options->maxrpm3 * (m_options->invert3 ? -1 : 1);
            }
            break;
        case 3:
            if (m_control.m4en && ! m_control.m4hold)
            {
                m_control.m4rpm = speed * m_options->maxrpm4 * (m_options->invert4 ? -1 : 1);
            }
            break;
        }
    };

    set_rpm(0, omega1);
    set_rpm(1, omega2);
    set_rpm(2, omega3);
    set_rpm(3, omega4);

    m_caterpillar->updateControl(&m_control);
}

void MainWindow::debugSimulatedEvent()
{
    auto x = ui->dbgx->text().toInt();
    auto y = ui->dbgy->text().toInt();
    auto yaw = ui->dbgz->text().toInt();

    sendMotorCommandControl(x, y, yaw, 100);
}


void MainWindow::joyChanged(JoystickStatus *status)
{
    auto a1 = status->axis_1 - m_options->jog_zero_x;
    auto a2 = status->axis_2 - m_options->jog_zero_y;
    auto a3 = status->axis_3 - m_options->jog_zero_yaw;
    qDebug() << a1 << a2 << a3;
    auto x = qAbs(a1) > AXIS_NOISE ? a1 : 0;
    auto y = qAbs(a2) > AXIS_NOISE ? a2 : 0;
    auto yaw = qAbs(a3) > AXIS_NOISE ? a3 : 0;
    if (x > AXIS_MAX) x = AXIS_MAX;
    if (x < AXIS_MIN) x = AXIS_MIN;
    if (y > AXIS_MAX) y = AXIS_MAX;
    if (y < AXIS_MIN) y = AXIS_MIN;
    if (yaw > AXIS_MAX) yaw = AXIS_MAX;
    if (yaw < AXIS_MIN) yaw = AXIS_MIN;
    if (m_options->enableInvZ)
        yaw = -yaw;
    if (x == 0 && y == 0 && yaw == 0)
    {
        // joystick not being operated
        //return;
    }

    sendMotorCommandControl(x, y, yaw, AXIS_MAX);
}


void MainWindow::zeroJog()
{
    auto last = m_joy->last();

    if (last == nullptr)
    {
        return;
    }

    qDebug() << "new ZERO axis coords: "
             << last->axis_1 << ", "
             << last->axis_2 << ", "
             << last->axis_3;
    m_options->jog_zero_x = last->axis_1;
    m_options->jog_zero_y = last->axis_2;
    m_options->jog_zero_yaw = last->axis_3;
    m_options->persist();
}


void MainWindow::enableChanged()
{
    m_control.en12v = ui->enable12v->checkState() == Qt::Checked;
    m_control.m1en = ui->enable1->checkState() == Qt::Checked;
    m_control.m2en = ui->enable2->checkState() == Qt::Checked;
    m_control.m3en = ui->enable3->checkState() == Qt::Checked;
    m_control.m4en = ui->enable4->checkState() == Qt::Checked;
    m_control.m1hold = ui->hold1->checkState() == Qt::Checked;
    m_control.m2hold = ui->hold2->checkState() == Qt::Checked;
    m_control.m3hold = ui->hold3->checkState() == Qt::Checked;
    m_control.m4hold = ui->hold4->checkState() == Qt::Checked;
    m_caterpillar->updateControl(&m_control, true);
}

void MainWindow::readUpdatedStatus(const ControlStatus *status)
{
    auto m1 = status->m1rpm;
    auto m2 = status->m2rpm;
    auto m3 = status->m3rpm;
    auto m4 = status->m4rpm;
    axes->setInfo(m1, m2, m3, m4);
}

void MainWindow::setLEDStatus()
{
    unsigned int mask = 0x0;
    mask |= ui->led_1->checkState() == Qt::Checked ? 0x001 : 0x00;
    mask |= ui->led_2->checkState() == Qt::Checked ? 0x002 : 0x00;
    mask |= ui->led_3->checkState() == Qt::Checked ? 0x004 : 0x00;
    mask |= ui->led_4->checkState() == Qt::Checked ? 0x008 : 0x00;
    mask |= ui->led_5->checkState() == Qt::Checked ? 0x010 : 0x00;
    mask |= ui->led_6->checkState() == Qt::Checked ? 0x020 : 0x00;
    mask |= ui->led_7->checkState() == Qt::Checked ? 0x040 : 0x00;
    mask |= ui->led_8->checkState() == Qt::Checked ? 0x080 : 0x00;
    mask |= ui->led_9->checkState() == Qt::Checked ? 0x100 : 0x00;
    m_caterpillar->set_leds(mask);
}

void MainWindow::performLEDPulse()
{
    unsigned int mask = 0x0;
    mask |= ui->led_1->checkState() == Qt::Checked ? 0x001 : 0x00;
    mask |= ui->led_2->checkState() == Qt::Checked ? 0x002 : 0x00;
    mask |= ui->led_3->checkState() == Qt::Checked ? 0x004 : 0x00;
    mask |= ui->led_4->checkState() == Qt::Checked ? 0x008 : 0x00;
    mask |= ui->led_5->checkState() == Qt::Checked ? 0x010 : 0x00;
    mask |= ui->led_6->checkState() == Qt::Checked ? 0x020 : 0x00;
    mask |= ui->led_7->checkState() == Qt::Checked ? 0x040 : 0x00;
    mask |= ui->led_8->checkState() == Qt::Checked ? 0x080 : 0x00;
    mask |= ui->led_9->checkState() == Qt::Checked ? 0x100 : 0x00;

    m_caterpillar->pulse_leds(mask,
                              ui->led_pulses_nr->text().toInt(),
                              ui->led_pulse_len->text().toInt(),
                              ui->led_pulse_del->text().toInt()
                              );
}


void MainWindow::setVideoFeed()
{
    quint8 camera = ui->video_camera->currentData().toInt();
    bool active = camera != 0;

    unsigned int id;

    id = ui->camera_resolution->currentData().toInt();
    auto resolution = resolutions[id];

    id = ui->camera_framerate->currentData().toInt();
    auto fr = framerates[id];

    id = ui->camera_shutter->currentData().toInt();
    auto sh = shutter_settings[id];

    m_caterpillar->set_video_feed(active, camera,
                                  ui->video_vflip->checkState() == Qt::Checked,
                                  ui->video_hflip->checkState() == Qt::Checked,
                                  resolution.width, resolution.height,
                                  fr.num, fr.den,
                                  sh.num, sh.den,
                                  ui->camera_iso->currentData().toInt(),
                                  DEFAULT_SETTLE_TIME,
                                  ui->video_record->checkState() == Qt::Checked
                                  );
}

void MainWindow::shootStillSequence()
{
    quint8 camera = ui->video_camera->currentData().toInt();
    bool active = camera != 0;

    unsigned int id;

    id = ui->camera_resolution->currentData().toInt();
    auto resolution = resolutions[id];

    id = ui->camera_framerate->currentData().toInt();
    auto fr = framerates[id];

    id = ui->camera_shutter->currentData().toInt();
    auto sh = shutter_settings[id];

    auto led_mask_of = [](
            QCheckBox *l1,
            QCheckBox *l2,
            QCheckBox *l3,
            QCheckBox *l4,
            QCheckBox *l5,
            QCheckBox *l6,
            QCheckBox *l7,
            QCheckBox *l8,
            QCheckBox *l9
            )
    {
        unsigned int mask = 0x0;
        mask |= l1->checkState() == Qt::Checked ? 0x001 : 0x00;
        mask |= l2->checkState() == Qt::Checked ? 0x002 : 0x00;
        mask |= l3->checkState() == Qt::Checked ? 0x004 : 0x00;
        mask |= l4->checkState() == Qt::Checked ? 0x008 : 0x00;
        mask |= l5->checkState() == Qt::Checked ? 0x010 : 0x00;
        mask |= l6->checkState() == Qt::Checked ? 0x020 : 0x00;
        mask |= l7->checkState() == Qt::Checked ? 0x040 : 0x00;
        mask |= l8->checkState() == Qt::Checked ? 0x080 : 0x00;
        mask |= l9->checkState() == Qt::Checked ? 0x100 : 0x00;
        return mask;
    };

    auto camera_nr_of = [](QComboBox *c)
    {
        return c->currentData().toInt();
    };

    quint8 mode = 0x0;
    if (ui->radio_video_port->isChecked()) mode=STILL_SEQUENCE_VIDEO_PORT;
    if (ui->radio_still_port->isChecked()) mode=STILL_SEQUENCE_STILL_PORT;
    if (ui->radio_long_exp->isChecked()) mode=STILL_SEQUENCE_LONG_EXPOSURE;

    unsigned int format = 0x00; // jpeg
    if (ui->radio_img_format_rgb->isChecked())
    {
        format = 0x01; // RGB raw
    }

    m_caterpillar->shoot_still_sequence(
                                  mode,
                                  ui->video_vflip->checkState() == Qt::Checked,
                                  ui->video_hflip->checkState() == Qt::Checked,
                                  resolution.width, resolution.height,
                                  fr.num, fr.den,
                                  sh.num, sh.den,
                                  ui->camera_iso->currentData().toInt(),
                                  DEFAULT_SETTLE_TIME,
                                  format,

                                  ui->led_pulses_nr->text().toInt(),
                                  ui->led_pulse_len->text().toInt(),
                                  ui->led_pulse_del->text().toInt(),
                                  camera_nr_of(ui->snap1_camera),
                                  led_mask_of(
                    ui->snap1_led_1,
                    ui->snap1_led_2,
                    ui->snap1_led_3,
                    ui->snap1_led_4,
                    ui->snap1_led_5,
                    ui->snap1_led_6,
                    ui->snap1_led_7,
                    ui->snap1_led_8,
                    ui->snap1_led_9
                                      ),
                                  camera_nr_of(ui->snap2_camera),
                                  led_mask_of(
                    ui->snap2_led_1,
                    ui->snap2_led_2,
                    ui->snap2_led_3,
                    ui->snap2_led_4,
                    ui->snap2_led_5,
                    ui->snap2_led_6,
                    ui->snap2_led_7,
                    ui->snap2_led_8,
                    ui->snap2_led_9
                                      ),
                                  camera_nr_of(ui->snap3_camera),
                                  led_mask_of(
                    ui->snap3_led_1,
                    ui->snap3_led_2,
                    ui->snap3_led_3,
                    ui->snap3_led_4,
                    ui->snap3_led_5,
                    ui->snap3_led_6,
                    ui->snap3_led_7,
                    ui->snap3_led_8,
                    ui->snap3_led_9
                                      ),
                                  camera_nr_of(ui->snap4_camera),
                                  led_mask_of(
                    ui->snap4_led_1,
                    ui->snap4_led_2,
                    ui->snap4_led_3,
                    ui->snap4_led_4,
                    ui->snap4_led_5,
                    ui->snap4_led_6,
                    ui->snap4_led_7,
                    ui->snap4_led_8,
                    ui->snap4_led_9
                                      ),
                                  0, 0,
                                  0, 0,
                                  0, 0,
                                  0, 0
                                  );

}


void MainWindow::openUrl_feed()
{
    QDesktopServices::openUrl(QUrl(ui->feed_label->text()));
}

void MainWindow::openUrl_still1()
{
    QDesktopServices::openUrl(QUrl(ui->still1_lbl->text()));
}

void MainWindow::openUrl_still2()
{
    QDesktopServices::openUrl(QUrl(ui->still2_lbl->text()));
}

void MainWindow::openUrl_still3()
{
    QDesktopServices::openUrl(QUrl(ui->still3_lbl->text()));
}

void MainWindow::openUrl_still4()
{
    QDesktopServices::openUrl(QUrl(ui->still4_lbl->text()));
}

