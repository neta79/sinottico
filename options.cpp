#include "options.h"
#include "ui_options.h"
#include <QSettings>

struct QIpAddressValidator : QValidator
{
    using QValidator::QValidator;

    State validate(QString &s, int &) const
    {
        return ip.setAddress(s) ? Acceptable : Intermediate;
    }

private:
    mutable QHostAddress ip;
};


OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);

    auto tabWidget = ui->tabWidget;
    auto tab = ui->tab;
    tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("OptionsDialog", "Drivers", 0));
    tabWidget->setTabText(tabWidget->indexOf(ui->tab_2), QApplication::translate("OptionsDialog", "Connection", 0));

    auto edits = {
        ui->accel1,
        ui->accel2,
        ui->accel3,
        ui->accel4,
        ui->decel1,
        ui->decel2,
        ui->decel3,
        ui->decel4,
        ui->maxrpm1,
        ui->maxrpm2,
        ui->maxrpm3,
        ui->maxrpm4,
        ui->portEdit,
        ui->videoApiPortEdit,
    };
    for (auto i: edits)
    {
        auto validator = new QIntValidator(1, 0xffff, i);
        i->setValidator(validator);
    }

    auto mappers = {
        ui->mapto1,
        ui->mapto2,
        ui->mapto3,
        ui->mapto4,
    };
    for (auto i: mappers)
    {
        i->addItem("M1", 0);
        i->addItem("M2", 0);
        i->addItem("M3", 0);
        i->addItem("M4", 0);
    }

    auto ipValidator = new QIpAddressValidator(ui->ipEdit);
    ui->ipEdit->setValidator(ipValidator);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

int OptionsDialog::exec(Options *o)
{
    ui->accel1 ->setText(QString::number(o->accel1));
    ui->accel2 ->setText(QString::number(o->accel2));
    ui->accel3 ->setText(QString::number(o->accel3));
    ui->accel4 ->setText(QString::number(o->accel4));
    ui->decel1 ->setText(QString::number(o->decel1));
    ui->decel2 ->setText(QString::number(o->decel2));
    ui->decel3 ->setText(QString::number(o->decel3));
    ui->decel4 ->setText(QString::number(o->decel4));
    ui->maxrpm1->setText(QString::number(o->maxrpm1));
    ui->maxrpm2->setText(QString::number(o->maxrpm2));
    ui->maxrpm3->setText(QString::number(o->maxrpm3));
    ui->maxrpm4->setText(QString::number(o->maxrpm4));
    ui->enable1->setChecked(o->enable1);
    ui->enable2->setChecked(o->enable2);
    ui->enable3->setChecked(o->enable3);
    ui->enable4->setChecked(o->enable4);
    ui->enableInvZ->setChecked(o->enableInvZ);
    ui->invert1->setChecked(o->invert1);
    ui->invert2->setChecked(o->invert2);
    ui->invert3->setChecked(o->invert3);
    ui->invert4->setChecked(o->invert4);
    ui->ipEdit->setText(o->host);
    ui->portEdit->setText(QString::number(o->port));
    ui->videoApiPortEdit->setText(QString::number(o->video_api_port));
    ui->mapto1->setCurrentIndex(o->map_to[0]);
    ui->mapto2->setCurrentIndex(o->map_to[1]);
    ui->mapto3->setCurrentIndex(o->map_to[2]);
    ui->mapto4->setCurrentIndex(o->map_to[3]);

    auto res = QDialog::exec();
    if (res != 0)
    {
        o->accel1 = ui->accel1->text().toInt();
        o->accel2 = ui->accel2->text().toInt();
        o->accel3 = ui->accel3->text().toInt();
        o->accel4 = ui->accel4->text().toInt();
        o->decel1 = ui->decel1->text().toInt();
        o->decel2 = ui->decel2->text().toInt();
        o->decel3 = ui->decel3->text().toInt();
        o->decel4 = ui->decel4->text().toInt();
        o->maxrpm1 = ui->maxrpm1->text().toInt();
        o->maxrpm2 = ui->maxrpm2->text().toInt();
        o->maxrpm3 = ui->maxrpm3->text().toInt();
        o->maxrpm4 = ui->maxrpm4->text().toInt();
        o->enable1 = ui->enable1->checkState() == Qt::Checked;
        o->enable2 = ui->enable2->checkState() == Qt::Checked;
        o->enable3 = ui->enable3->checkState() == Qt::Checked;
        o->enable4 = ui->enable4->checkState() == Qt::Checked;
        o->enableInvZ = ui->enableInvZ->checkState() == Qt::Checked;
        o->invert1 = ui->invert1->checkState() == Qt::Checked;
        o->invert2 = ui->invert2->checkState() == Qt::Checked;
        o->invert3 = ui->invert3->checkState() == Qt::Checked;
        o->invert4 = ui->invert4->checkState() == Qt::Checked;
        o->host = ui->ipEdit->text();
        o->port = ui->portEdit->text().toInt();
        o->video_api_port = ui->videoApiPortEdit->text().toInt();
        o->map_to[0] = ui->mapto1->currentIndex();
        o->map_to[1] = ui->mapto2->currentIndex();
        o->map_to[2] = ui->mapto3->currentIndex();
        o->map_to[3] = ui->mapto4->currentIndex();
    }
    return res;
}


void Options::load()
{
    QSettings settings;

    enable1 = settings.value("drivers/enable1", true).toBool();
    enable2 = settings.value("drivers/enable2", true).toBool();
    enable3 = settings.value("drivers/enable3", true).toBool();
    enable4 = settings.value("drivers/enable4", true).toBool();
    enableInvZ = settings.value("axis/invert_yaw", false).toBool();
    invert1 = settings.value("drivers/invert1", false).toBool();
    invert2 = settings.value("drivers/invert2", false).toBool();
    invert3 = settings.value("drivers/invert3", false).toBool();
    invert4 = settings.value("drivers/invert4", false).toBool();

    accel1 = settings.value("drivers/accel1", 0x2e).toInt();
    accel2 = settings.value("drivers/accel2", 0x2e).toInt();
    accel3 = settings.value("drivers/accel3", 0x2e).toInt();
    accel4 = settings.value("drivers/accel4", 0x2e).toInt();
    decel1 = settings.value("drivers/decel1", 0x2e).toInt();
    decel2 = settings.value("drivers/decel2", 0x2e).toInt();
    decel3 = settings.value("drivers/decel3", 0x2e).toInt();
    decel4 = settings.value("drivers/decel4", 0x2e).toInt();

    maxrpm1 = settings.value("drivers/maxrpm1", 8000).toInt();
    maxrpm2 = settings.value("drivers/maxrpm2", 8000).toInt();
    maxrpm3 = settings.value("drivers/maxrpm3", 8000).toInt();
    maxrpm4 = settings.value("drivers/maxrpm4", 8000).toInt();

    map_to[0] = settings.value("drivers/map_thrust_to_motor", 0).toInt();
    map_to[1] = settings.value("drivers/map_pitch_positive_to_motor", 1).toInt();
    map_to[2] = settings.value("drivers/map_pitch_negative_to_motor", 2).toInt();
    map_to[3] = settings.value("drivers/map_yaw_to_motor", 3).toInt();

    host = settings.value("network/host", "192.168.68.1").toString();
    port = settings.value("network/port", 3135).toInt();
    video_api_port = settings.value("network/video_api_port", 3136).toInt();

    jog_zero_x = settings.value("axis/x_zero", 0).toInt();
    jog_zero_y = settings.value("axis/y_zero", 0).toInt();
    jog_zero_yaw = settings.value("axis/yax_zero", 0).toInt();
}

void Options::persist()
{
    QSettings settings;

    settings.setValue("drivers/enable1", enable1);
    settings.setValue("drivers/enable2", enable2);
    settings.setValue("drivers/enable3", enable3);
    settings.setValue("drivers/enable4", enable4);
    settings.setValue("axis/invert_yaw", enableInvZ);

    settings.setValue("drivers/invert1", invert1);
    settings.setValue("drivers/invert2", invert2);
    settings.setValue("drivers/invert3", invert3);
    settings.setValue("drivers/invert4", invert4);

    settings.setValue("drivers/accel1", accel1);
    settings.setValue("drivers/accel2", accel2);
    settings.setValue("drivers/accel3", accel3);
    settings.setValue("drivers/accel4", accel4);
    settings.setValue("drivers/decel1", decel1);
    settings.setValue("drivers/decel2", decel2);
    settings.setValue("drivers/decel3", decel3);
    settings.setValue("drivers/decel4", decel4);

    settings.setValue("drivers/maxrpm1", maxrpm1);
    settings.setValue("drivers/maxrpm2", maxrpm2);
    settings.setValue("drivers/maxrpm3", maxrpm3);
    settings.setValue("drivers/maxrpm4", maxrpm4);

    settings.setValue("drivers/map_thrust_to_motor", map_to[0]);
    settings.setValue("drivers/map_pitch_positive_to_motor", map_to[1]);
    settings.setValue("drivers/map_pitch_negative_to_motor", map_to[2]);
    settings.setValue("drivers/map_yaw_to_motor", map_to[3]);

    settings.setValue("network/host", host);
    settings.setValue("network/port", port);
    settings.setValue("network/video_api_port", video_api_port);

    settings.setValue("axis/x_zero", jog_zero_x);
    settings.setValue("axis/y_zero", jog_zero_y);
    settings.setValue("axis/yaw_zero", jog_zero_yaw);
}
